import styled from "styled-components";

export const EntryContainer = styled.div`
  display: flex;
  border-width: 1px;
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
`;

export const EntryTitle = styled.h2`
  color: red;
`;
