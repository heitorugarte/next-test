import React from "react";
import { Word } from "../../types/word";
import { EntryContainer, EntryTitle } from "./styles";

interface IWordEntryProps {
  entry: Word;
}

const WordEntry: React.FC<IWordEntryProps> = ({ entry }) => {
  return (
    <EntryContainer>
      <EntryTitle>{entry.word}</EntryTitle>
      <EntryTitle>{entry.score}</EntryTitle>
    </EntryContainer>
  );
};

export default WordEntry;
