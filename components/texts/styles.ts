import styled from "styled-components";

interface IDefaultTextProps {
  fontSize: number;
  color: string;
  textAlign: string;
  fontWeight: string;
}

const DefaultParagraph = styled.p<IDefaultTextProps>`
  font-size: ${(props) => (props.fontSize ? props.fontSize : 14)}px;
  color: ${(props) => (props.color ? props.color : "black")};
  text-align: ${(props) => (props.textAlign ? props.textAlign : "left")};
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : "normal")};
`;

export default DefaultParagraph;
