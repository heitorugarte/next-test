import React from "react";
import DefaultParagraph from "../../texts/styles";
import { PrimaryButtonContainer } from "./styles";

interface IPrimaryButtonProps {
  label: string;
  onClick: Function;
}

const PrimaryButton: React.FC<IPrimaryButtonProps> = ({ label, onClick }) => {
  return (
    <PrimaryButtonContainer onClick={() => onClick()}>
      <DefaultParagraph textAlign="center" color="black" fontSize={18}>
        {label}
      </DefaultParagraph>
    </PrimaryButtonContainer>
  );
};

export default PrimaryButton;
