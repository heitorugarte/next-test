import styled from "styled-components";

export const PrimaryButtonContainer = styled.button`
  background-color: #ccc;
  border-color: black;
  border-radius: 5px;
  margin: 5px;
`;
