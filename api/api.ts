import axios from "axios";

const api = axios.create({
  baseURL: "https://api.datamuse.com",
  timeout: 5000,
});

const searchWord = (query: string, searchType: string) => {
  return api.get("/words", { params: { [searchType]: query } });
};

export { searchWord as searchWordSoundsLike };
