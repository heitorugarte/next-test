import styled from "styled-components";

export const DefaultContainer = styled.div`
  justify-content: center;
  align-items: center;
  width: 90%;
`;
