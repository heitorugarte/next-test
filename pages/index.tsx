import Head from "next/head";
import React, { useState } from "react";
import styles from "../styles/Home.module.css";
import PrimaryButton from "../components/buttons/primary_button";
import { useRouter } from "next/router";

export default function Home() {
  const [word, setWord] = useState("");
  const router = useRouter();

  return (
    <div className={styles.container}>
      <Head>
        <title>Words App - Heitor</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <h1>Words App</h1>
        <form style={{ display: "flex", flexDirection: "column" }}>
          <input
            placeholder="Palavra (em inglês)..."
            onChange={(e) => setWord(e.target.value)}
          />
          <PrimaryButton
            label="Sounds Like..."
            onClick={() =>
              router.push({
                pathname: "/results",
                query: { word: word, searchType: "sl" },
              })
            }
          />
          <PrimaryButton
            label="Means Like..."
            onClick={() =>
              router.push({
                pathname: "/results",
                query: { word: word, searchType: "ml" },
              })
            }
          />
        </form>
      </main>
    </div>
  );
}
