import Head from "next/head";
import { useRouter } from "next/router";
import WordEntry from "../../components/word_entry";
import { useWordSWR } from "../../swr/useWordSwr";

export default function Results() {
  const router = useRouter();
  const { word, searchType } = router.query;
  const { data } = useWordSWR(word, searchType);
  if (!word) router.push("/");

  return data && word ? (
    <div>
      <Head>
        <title>Resultados ({word})</title>
      </Head>
      <p>
        {searchType === "sl" ? "Sounds like: " : "Means like: "} {word}
      </p>
      {data.map((item) => {
        return <WordEntry entry={item} />;
      })}
    </div>
  ) : null;
}
