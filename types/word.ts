export type Word = {
  word: string;
  score: number;
  numSyllabes: number;
};
