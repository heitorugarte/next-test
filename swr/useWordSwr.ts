import useSWR from "swr";
import { searchWordSoundsLike } from "../api/api";
import { Word } from "../types/word";

const useWordSWR = (word: string | string[], searchType: string | string[]) => {
  let wordObj = word as string;
  let searchTypeObj = searchType as string;
  return useSWR<Word[], Error>("/words", async () => {
    const response = await searchWordSoundsLike(wordObj, searchTypeObj);
    return response.data;
  });
};

export { useWordSWR };
